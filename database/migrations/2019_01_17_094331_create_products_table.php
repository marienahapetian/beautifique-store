<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(false);
            $table->string('name');
            $table->string('slug');
            $table->string('sku');
            $table->unsignedInteger('stockQty');
            $table->float('price');
            $table->boolean('onSale')->default(false);
            $table->float('salePrice')->default(null);
            $table->string('img')->default(null);
            $table->string('thumbnails')->default(null);
            $table->longText('description')->default(null);
            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
