@include('inc.header')

<body class="header_sticky header-style-2 has-menu-extra">
<!-- Preloader -->
<!-- Boxed -->
<div class="boxed">
    <div id="site-header-wrap">
        <!-- Header -->
        <header id="header" class="header clearfix">
            <div class="container-fluid clearfix container-width-93" id="site-header-inner">
                <div id="logo" class="logo float-left">
                    <a href="/" title="logo">
                        <img src="{{asset('img/logo.png')}}" alt="image" width="107" height="24" data-retina="{{asset('img/logo@2x.png')}}"
                             data-width="107" data-height="24">
                    </a>
                </div><!-- /.logo -->
                <div class="mobile-button"><span></span></div>
                <ul class="menu-extra">
                    <li class="box-search">
                        <a class="icon_search header-search-icon" href="#"></a>
                        <form role="search" method="get" class="header-search-form" action="#">
                            <input type="text" value="" name="s" class="header-search-field" placeholder="Search...">
                            <button type="submit" class="header-search-submit" title="Search">Search</button>
                        </form>
                    </li>
                    <li class="box-login">
                        <a class="icon_login" href="#"></a>
                    </li>
                    <li class="box-cart nav-top-cart-wrapper">
                        <a class="icon_cart nav-cart-trigger active" href="#"><span>3</span></a>
                        <div class="nav-shop-cart">
                            <div class="widget_shopping_cart_content">
                                <div class="woocommerce-min-cart-wrap">
                                    <ul class="woocommerce-mini-cart cart_list product_list_widget ">
                                        <li class="woocommerce-mini-cart-item mini_cart_item">
                                            <span>No Items in Shopping Cart</span>
                                        </li>
                                    </ul>
                                </div><!-- /.widget_shopping_cart_content -->
                            </div>
                        </div><!-- /.nav-shop-cart -->
                    </li>
                </ul><!-- /.menu-extra -->
                <div class="nav-wrap">
                    <!-- /.mainnav -->
                </div><!-- /.nav-wrap -->
            </div>
            <nav id="mainnav-mobi" class="mainnav" style="display: none;">
                <ul class="menu">
                    <li>
                        <a href="/">HOME</a><span class="btn-submenu"></span>
                        <ul class="submenu" style="display: none;">
                            <li><a href="/">Homepage Style 1</a></li>
                            <li><a href="index-v2.html">Homepage Style 2</a></li>
                            <li><a href="index-v3.html">Homepage Style 3</a></li>
                            <li><a href="index-v4.html">Homepage Style 4</a></li>
                            <li><a href="index-v5.html">Homepage Style 5</a></li>
                            <li><a href="index-v6.html">Homepage Style 6</a></li>
                            <li><a href="index-v7.html">Homepage Style 7</a></li>
                            <li><a href="index-v8.html">Homepage Style 8</a></li>
                            <li><a href="index-v9.html">Homepage Style 9</a></li>
                            <li><a href="index-v10.html">Homepage Style 10</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="shop-3col.html">SHOP</a><span class="btn-submenu"></span>
                        <ul class="submenu" style="display: none;">
                            <li>
                                <a href="shop-3col.html">Shop Layouts</a><span class="btn-submenu"></span>
                                <ul class="submenu" style="display: none;">
                                    <li><a href="shop-3col.html">Three Columns</a></li>
                                    <li><a href="shop-4col.html">Four Columns</a></li>
                                    <li><a href="shop-5col.html">Five Columns</a></li>
                                    <li><a href="shop-3col-slide.html">Slidebar Three Columns</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="shop-detail-des.html">Shop Details</a><span class="btn-submenu"></span>
                                <ul class="submenu" style="display: none;">
                                    <li><a href="shop-detail-des.html">Details Description</a></li>
                                    <li><a href="shop-detail-exter.html">Details External</a></li>
                                    <li><a href="shop-detail-option.html">Details Options</a></li>
                                    <li><a href="shop-detail-fix.html">Details Fix</a></li>
                                    <li><a href="shop-detail-zoom.html">Details Zoom</a></li>
                                    <li><a href="shop-detail-group.html">Details Grouped</a></li>
                                    <li><a href="shop-detail-video.html">Details Video</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="coming-soon.html">PAGE</a><span class="btn-submenu"></span>
                        <ul class="submenu" style="display: none;">

                            <li><a href="coming-soon.html">Coming Soon</a></li>
                            <li><a href="404.html"> Error 404</a></li>
                            <li><a href="faqs.html">FAQs</a></li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="blog-list.html">BLOG</a><span class="btn-submenu"></span>
                        <ul class="submenu" style="display: none;">
                            <li><a href="blog-list.html">Blog List Full</a></li>
                            <li><a href="blog-list-1.html">Blog list Slide 1</a></li>
                            <li><a href="blog-list-2.html">Blog list Slide 2</a></li>
                            <li class="active"><a href="blog-grid.html">Blog Gird Full</a></li>
                            <li><a href="blog-grid-1.html">Blog Gird Slide</a></li>
                            <li><a href="blog-detail.html">Blog Details</a></li>
                        </ul><!-- /.submenu -->
                    </li>
                    <li>
                        <a href="contact.html">CONTACT</a><span class="btn-submenu"></span>
                        <ul class="submenu right-submenu" style="display: none;">
                            <li><a href="contact.html">Contact Style 1</a></li>
                            <li><a href="contact-v2.html">Contact Style 2</a></li>
                        </ul>
                    </li>
                </ul>
            </nav><!-- /.container-fluid -->
        </header>
        <div style="height: 89px; display: none;"></div><!-- /header -->
    </div><!-- /.site-header-wrap -->

    <!-- Page title -->
    <div class="page-title parallax parallax1" style="background-position: 50% 66px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h1 class="title">Blog List</h1>
                    </div><!-- /.page-title-heading -->
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="blog-grid.html">Blog</a></li>
                        </ul>
                    </div><!-- /.breadcrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-title -->

    <!-- Blog posts -->
    <section class="blog-posts grid-posts">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="post-wrap margin-bottom-26">
                        <div class="grid three">

                            <article class="post clearfix">
                                <div class="featured-post">
                                    <img src="{{asset('img/11.jpg')}}" alt="image">
                                </div>
                                <div class="content-post">
                                    <div class="title-post">
                                        <h2><a href="blog-detail.html">Grenfell Remembered, Six Months On</a></h2>
                                    </div><!-- /.title-post -->
                                    <div class="entry-post">
                                        <p>MARKING exactly six months since the devastating blaze at Grenfell Tower, in
                                            which 71 people died and hundreds more lost...</p>
                                        <div class="more-link">
                                            <a href="blog-detail.html">Read More</a>
                                        </div>
                                    </div>
                                </div><!-- /.content-post -->
                            </article><!-- /.post -->

                            <article class="post clearfix">
                                <div class="featured-post">
                                    <img src="{{asset('img/12.jpg')}}" alt="image">
                                </div>
                                <div class="content-post">
                                    <div class="title-post">
                                        <h2><a href="blog-detail.html">The Design Museum Honours...</a></h2>
                                    </div><!-- /.title-post -->
                                    <div class="entry-post">
                                        <p>When the Tunisian-born couturier Azzedine Alaïa passed away in Paris on
                                            November 18, tributes began pouring in...</p>
                                        <div class="more-link">
                                            <a href="blog-detail.html">Read More</a>
                                        </div>
                                    </div>
                                </div><!-- /.content-post -->
                            </article><!-- /.post -->

                            <article class="post clearfix">
                                <div class="featured-post">
                                    <img src="{{asset('img/13.jpg')}}" alt="image">
                                </div>
                                <div class="content-post">
                                    <div class="title-post">
                                        <h2><a href="blog-detail.html">Is Adriana Lima Hanging Up Her Wings?</a></h2>
                                    </div><!-- /.title-post -->
                                    <div class="entry-post">
                                        <p>It is a long established fact that a reader will be dis- tracted by the
                                            readable content of a page when looking at its layout...</p>
                                        <div class="more-link">
                                            <a href="blog-detail.html">Read More</a>
                                        </div>
                                    </div>
                                </div><!-- /.content-post -->
                            </article><!-- /.post -->

                            <article class="post clearfix">
                                <div class="featured-post margin-bottom-14">
                                    <img src="{{asset('img/14.jpg')}}" alt="image">
                                </div>
                                <div class="content-post">
                                    <div class="title-post">
                                        <h2><a href="blog-detail.html">Looking For A New Statement Piece?</a></h2>
                                    </div><!-- /.title-post -->
                                    <div class="entry-post">
                                        <p>There’s nothing more personal in anyone’s wardrobe than their jewellery. It
                                            tells a story beyond the possi- bilities of most clothes...</p>
                                        <div class="more-link">
                                            <a href="blog-detail.html">Read More</a>
                                        </div>
                                    </div>
                                </div><!-- /.content-post -->
                            </article><!-- /.post -->

                            <article class="post clearfix">
                                <div class="featured-post margin-bottom-14">
                                    <img src="{{asset('img/15.jpg')}}" alt="image">
                                </div>
                                <div class="content-post">
                                    <div class="title-post">
                                        <h2><a href="blog-detail.html">Anya Hindmarch Changes Strategy</a></h2>
                                    </div><!-- /.title-post -->
                                    <div class="entry-post">
                                        <p>ANYA HINDMARCH’s shows are a highlight on the London Fashion Week schedule -
                                            a visual feast that has seen the designer's team...</p>
                                        <div class="more-link">
                                            <a href="blog-detail.html">Read More</a>
                                        </div>
                                    </div>
                                </div><!-- /.content-post -->
                            </article><!-- /.post -->

                            <article class="post clearfix">
                                <div class="featured-post margin-bottom-14">
                                    <img src="{{asset('img/16.jpg')}}" alt="image">
                                </div>
                                <div class="content-post">
                                    <div class="title-post">
                                        <h2><a href="blog-detail.html">This Week In Pictures - 11/12/2018</a></h2>
                                    </div><!-- /.title-post -->
                                    <div class="entry-post">
                                        <p>Democrat Doug Jones defeats Roy Moore to win a US senate seat for Alabama.
                                            The unexpected victory has made Jones the first...</p>
                                        <div class="more-link">
                                            <a href="blog-detail.html">Read More</a>
                                        </div>
                                    </div>
                                </div><!-- /.content-post -->
                            </article><!-- /.post -->

                            <article class="post clearfix">
                                <div class="featured-post">
                                    <img src="{{asset('img/17.jpg')}}" alt="image">
                                </div>
                                <div class="content-post">
                                    <div class="title-post">
                                        <h2><a href="blog-detail.html">Five Festive Obsessions That Make...</a></h2>
                                    </div><!-- /.title-post -->
                                    <div class="entry-post">
                                        <p>‘Tis the season of merriment, as the December calendar swells with soirées,
                                            parties and gatherings galore. The ingredients of each family...</p>
                                        <div class="more-link">
                                            <a href="blog-detail.html">Read More</a>
                                        </div>
                                    </div>
                                </div><!-- /.content-post -->
                            </article><!-- /.post -->

                            <article class="post clearfix">
                                <div class="featured-post">
                                    <img src="{{asset('img/18.jpg')}}" alt="image">
                                </div>
                                <div class="content-post">
                                    <div class="title-post">
                                        <h2><a href="blog-detail.html">The Sustainable New Denim Line</a></h2>
                                    </div><!-- /.title-post -->
                                    <div class="entry-post">
                                        <p>There is a new era of sustainability in fashion. No longer is such a word
                                            exclusively associated with hand-hewn hemp or hessian sacking,..</p>
                                        <div class="more-link">
                                            <a href="blog-detail.html">Read More</a>
                                        </div>
                                    </div>
                                </div><!-- /.content-post -->
                            </article><!-- /.post -->

                            <article class="post clearfix">
                                <div class="featured-post">
                                    <img src="{{asset('img/19.jpg')}}" alt="image">
                                </div>
                                <div class="content-post">
                                    <div class="title-post">
                                        <h2><a href="blog-detail.html">How Saint Laurent’s Crystal Boots...</a></h2>
                                    </div><!-- /.title-post -->
                                    <div class="entry-post">
                                        <p>BEYONCÉ might have been out of the spotlight since releasing her sixth studio
                                            album, Lemonade, and nursing twins Rumi and Sir...</p>
                                        <div class="more-link">
                                            <a href="blog-detail.html">Read More</a>
                                        </div>
                                    </div>
                                </div><!-- /.content-post -->
                            </article><!-- /.post -->

                        </div><!-- /.grid -->
                    </div><!-- /.post-wrap -->
                    <div class="blog-pagination text-center clearfix">
                        <ul class="flat-pagination">
                            <li class="prev">
                                <a href="#"><i class="fa fa-angle-left"></i></a>
                            </li>
                            <li><a href="#">1</a></li>
                            <li class="active"><a href="#" title="">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        </ul><!-- /.flat-pagination -->
                    </div><!-- /.blog-pagination -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.blog posts -->

    <section class="flat-row mail-chimp">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="text">
                        <h3>Sign up for Send Newsletter</h3>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="subscribe clearfix">
                        <form action="#" method="post" accept-charset="utf-8" id="subscribe-form">
                            <div class="subscribe-content">
                                <div class="input">
                                    <input type="email" name="subscribe-email" placeholder="Your Email">
                                </div>
                                <div class="button">
                                    <button type="button">SUBCRIBE</button>
                                </div>
                            </div>
                        </form>
                        <ul class="flat-social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                            <li><a href="#"><i class="fa fa-behance"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul><!-- /.flat-social -->
                    </div><!-- /.subscribe -->
                </div>
            </div>
        </div>
    </section><!-- /.mail-chimp -->

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="widget widget-link">
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Online Store</a></li>
                            <li><a href="blog-list.html">Blog</a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                        </ul>
                    </div><!-- /.widget -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-3">
                    <div class="widget widget-link link-login">
                        <ul>
                            <li><a href="#">Login/ Register</a></li>
                            <li><a href="#">Your Cart</a></li>
                            <li><a href="#">Wishlist items</a></li>
                            <li><a href="#">Your checkout</a></li>
                        </ul>
                    </div><!-- /.widget -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-3">
                    <div class="widget widget-link link-faq">
                        <ul>
                            <li><a href="faqs.html">FAQs</a></li>
                            <li><a href="#">Term of service</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Returns</a></li>
                        </ul>
                    </div><!-- /.widget -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-3">
                    <div class="widget widget-brand">
                        <div class="logo logo-footer">
                            <a href="/"><img src="{{asset('img/logo@2x.png')}}" alt="image" width="107" height="24"></a>
                        </div>
                        <ul class="flat-contact">
                            <li class="address">112 Kingdom, NA 12, New York</li>
                            <li class="phone">+12 345 678 910</li>
                            <li class="email">infor.deercreative@gmail.com</li>
                        </ul><!-- /.flat-contact -->
                    </div><!-- /.widget -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </footer><!-- /.footer -->

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="copyright text-center">Copyright @2018 <a href="#">Modaz</a></p>
                </div>
            </div>
        </div>
    </div>

    <!-- Go Top -->
    <a class="go-top">
        <i class="fa fa-chevron-up"></i>
    </a>

</div>

@include('inc.footer')