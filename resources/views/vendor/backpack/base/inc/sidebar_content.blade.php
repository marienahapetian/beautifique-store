<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fas fa-tachometer-alt"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href='{{ backpack_url('order') }}'><i class="fas fa-shopping-cart"></i> <span>Orders</span></a></li>
<li><a href='{{ backpack_url('product') }}'><i class="fas fa-box-open"></i> <span>Products</span></a></li>
<li><a href='{{ backpack_url('brand') }}'><i class="fas fa-copyright"></i> <span>Brands</span></a></li>
<li><a href='{{ backpack_url('category') }}'><i class="fas fa-sitemap"></i> <span>Categories</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fas fa-copy"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="{{ backpack_url('user') }}"><i class="far fa-user"></i> <span>Users</span></a></li>
      <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
      <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
    </ul>
  </li>
<li><a href='{{ backpack_url('tag') }}'><i class='fa fa-tag'></i> <span>Tags</span></a></li>