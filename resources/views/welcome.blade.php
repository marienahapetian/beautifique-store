@include('inc.header')
<body class="header_sticky header-style-1 has-menu-extra">

<!-- Preloader -->
<div id="loading-overlay">
    <div class="loader"></div>
</div>

<!-- Boxed -->
<div class="boxed">
    <div id="site-header-wrap">
        <!-- Header -->
        <header id="header" class="header header-container clearfix is-fixed is-small">
            <div class="container clearfix" id="site-header-inner">
                <div id="logo" class="logo float-left">
                    <a href="index.html" title="logo">
                        <img src="{{asset('img/logo.png')}}" alt="image" width="107" height="24" data-retina="{{asset('img/logo@2x.png')}}"
                             data-width="107" data-height="24">
                    </a>
                </div>
                <!-- /.logo -->
                <div class="mobile-button"><span></span></div>
                <ul class="menu-extra">
                    <li class="box-search">
                        <a class="icon_search header-search-icon" href="#"></a>

                        <form role="search" method="get" class="header-search-form" action="#">
                            <input type="text" value="" name="s" class="header-search-field" placeholder="Search...">
                            <button type="submit" class="header-search-submit" title="Search">Search</button>
                        </form>
                    </li>
                    <li class="box-login">
                        <a class="icon_login" href="#"></a>
                    </li>
                    <li class="box-cart nav-top-cart-wrapper">
                        <a class="icon_cart nav-cart-trigger active" href="#"><span>3</span></a>

                        <div class="nav-shop-cart">
                            <div class="widget_shopping_cart_content">
                                <div class="woocommerce-min-cart-wrap">
                                    <ul class="woocommerce-mini-cart cart_list product_list_widget ">
                                        <li class="woocommerce-mini-cart-item mini_cart_item">
                                            <span>No Items in Shopping Cart</span>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.widget_shopping_cart_content -->
                            </div>
                        </div>
                        <!-- /.nav-shop-cart -->
                    </li>
                </ul>
                <!-- /.menu-extra -->
                <div class="nav-wrap">
                    <nav id="mainnav" class="mainnav">
                        <ul class="menu">
                            <li>
                                <a href="/shop">SHOP</a>
                            </li>
                            <li>
                                <a href="/blog">BLOG</a>
                            </li>
                            <li>
                                <a href="/contact">CONTACT</a>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.mainnav -->
                </div>
                <!-- /.nav-wrap -->
            </div>
            <!-- /.container-fluid -->
        </header>
        <div style="height: 89px; display: block;"></div>
        <!-- /header -->
    </div>
    <!-- /#site-header-wrap -->

    <!-- SLIDER -->
    <div class="forcefullwidth_wrapper_tp_banner"
         style="position:relative;width:100%;height:auto;margin-top:0px;margin-bottom:0px">
        <div class="rev_slider_wrapper fullwidthbanner-container">
            <div id="rev-slider1" class="rev_slider" data-version="5.4.5" style="display:none">
                <ul class="tp-revslider-mainul">
                    <!-- Slide 1 -->
                    <li data-transition="random" class="tp-revslider-slidesli active-revslide">
                        <img src="{{asset('img/slider-bg-1.jpg')}}" class="rev-slidebg">
                    </li>
                    <!-- /End Slide 1 -->

                    <!-- Slide 2 -->
                    <li data-transition="random" class="tp-revslider-slidesli">
                        <img src="{{asset('img/slider-bg-5.jpg')}}" class="rev-slidebg">
                    </li>
                    <!-- /End Slide 2 -->

                </ul>
            </div>
        </div>
    </div>
    <!-- END SLIDER -->

    <!-- IMAGE BOX -->
    <section class="flat-row no-padding">
        <div class="container-fluid">
            <div class="row equal sm-equal-auto">
                <div class="col-md-6 half-background style-2" style="height: 719px;">
                    <div class="flat-image-box style-1 clearfix">
                        <div class="elm-btn">
                            <a href="#" class="themesflat-button bg-white width-150">For Men’s</a>
                        </div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6" style="height: 719px;">
                    <div class="product-content clearfix">
                        <div class="divider h160"></div>
                        <div class="flat-content-box clearfix" data-margin="0 0px 0 0px" data-mobilemargin="0 0 0 0"
                             style="margin:0 0px 0 0px">
                            <div class="product style3 flat-carousel-box has-bullets bullet-circle bullet40 clearfix"
                                 data-auto="false" data-column="3" data-column2="1" data-column3="1" data-gap="0">
                                <div class="owl-carousel owl-theme owl-loaded owl-drag">
                                            <div class="owl-item active" style="width: 215px;">
                                                <div class="product-item">
                                                    <div class="product-thumb clearfix">
                                                        <a href="#">
                                                            <img src="{{asset('img/10.jpg')}}" alt="image">
                                                        </a>
                                                        <span class="new">New</span>
                                                    </div>
                                                    <div class="product-info clearfix">
                                                        <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                        <div class="price">
                                                            <ins>
                                                                <span class="amount">$19.00</span>
                                                            </ins>
                                                        </div>
                                                        <ul class="flat-color-list width-14">
                                                            <li>
                                                                <a href="#" class="red"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="blue"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="black"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="add-to-cart text-center">
                                                        <a href="#">ADD TO CART</a>
                                                    </div>
                                                    <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                                </div>
                                            </div>
                                    <div class="owl-item" style="width: 215px;">
                                        <div class="product-item">
                                            <div class="product-thumb clearfix">
                                                <a href="#">
                                                    <img src="{{asset('img/13.jpg')}}" alt="image">
                                                </a>
                                                <span class="new">New</span>
                                            </div>
                                            <div class="product-info clearfix">
                                                <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                <div class="price">
                                                    <ins>
                                                        <span class="amount">$19.00</span>
                                                    </ins>
                                                </div>
                                                <ul class="flat-color-list width-14">
                                                    <li>
                                                        <a href="#" class="red"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="blue"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="black"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="add-to-cart text-center">
                                                <a href="#">ADD TO CART</a>
                                            </div>
                                            <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                        </div>
                                    </div><!--owl item end -->
                                    <div class="owl-item" style="width: 215px;">
                                        <div class="product-item">
                                            <div class="product-thumb clearfix">
                                                <a href="#">
                                                    <img src="{{asset('img/10.jpg')}}" alt="image">
                                                </a>
                                                <span class="new">New</span>
                                            </div>
                                            <div class="product-info clearfix">
                                                <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                <div class="price">
                                                    <ins>
                                                        <span class="amount">$19.00</span>
                                                    </ins>
                                                </div>
                                                <ul class="flat-color-list width-14">
                                                    <li>
                                                        <a href="#" class="red"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="blue"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="black"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="add-to-cart text-center">
                                                <a href="#">ADD TO CART</a>
                                            </div>
                                            <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                        </div>
                                    </div><!--owl item end -->
                                </div><!-- /.owl-carousel  -->
                            </div>
                        </div>
                        <!-- /.flat-content-box -->
                        <div class="divider h97"></div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- END IMAGE BOX -->

    <!-- IMAGE BOX -->
    <section class="flat-row no-padding">
        <div class="container-fluid">
            <div class="row equal sm-equal-auto">
                <div class="col-md-6" style="height: 719px;">
                    <div class="product-content clearfix">
                        <div class="divider h160"></div>
                        <div class="flat-content-box clearfix" data-margin="0 0px 0 0px" data-mobilemargin="0 0 0 0"
                             style="margin:0 0px 0 0px">
                            <div class="product style3 flat-carousel-box has-bullets bullet-circle bullet40 clearfix"
                                 data-auto="false" data-column="3" data-column2="1" data-column3="1" data-gap="0">
                                <div class="owl-carousel owl-theme owl-loaded owl-drag">
                                            <div class="owl-item" style="width: 215px;">
                                                <div class="product-item">
                                                    <div class="product-thumb clearfix">
                                                        <a href="#">
                                                            <img src="{{asset('img/13.jpg')}}" alt="image">
                                                        </a>
                                                        <span class="new">New</span>
                                                    </div>
                                                    <div class="product-info clearfix">
                                                        <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                        <div class="price">
                                                            <ins>
                                                                <span class="amount">$19.00</span>
                                                            </ins>
                                                        </div>
                                                        <ul class="flat-color-list width-14">
                                                            <li>
                                                                <a href="#" class="red"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="blue"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="black"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="add-to-cart text-center">
                                                        <a href="#">ADD TO CART</a>
                                                    </div>
                                                    <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                                </div>
                                            </div><!--owl item end -->
                                    <div class="owl-item" style="width: 215px;">
                                        <div class="product-item">
                                            <div class="product-thumb clearfix">
                                                <a href="#">
                                                    <img src="{{asset('img/12.jpg')}}" alt="image">
                                                </a>
                                                <span class="new">New</span>
                                            </div>
                                            <div class="product-info clearfix">
                                                <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                <div class="price">
                                                    <ins>
                                                        <span class="amount">$19.00</span>
                                                    </ins>
                                                </div>
                                                <ul class="flat-color-list width-14">
                                                    <li>
                                                        <a href="#" class="red"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="blue"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="black"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="add-to-cart text-center">
                                                <a href="#">ADD TO CART</a>
                                            </div>
                                            <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                        </div>
                                    </div><!--owl item end -->
                                    <div class="owl-item" style="width: 215px;">
                                        <div class="product-item">
                                            <div class="product-thumb clearfix">
                                                <a href="#">
                                                    <img src="{{asset('img/11.jpg')}}" alt="image">
                                                </a>
                                                <span class="new">New</span>
                                            </div>
                                            <div class="product-info clearfix">
                                                <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                <div class="price">
                                                    <ins>
                                                        <span class="amount">$19.00</span>
                                                    </ins>
                                                </div>
                                                <ul class="flat-color-list width-14">
                                                    <li>
                                                        <a href="#" class="red"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="blue"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="black"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="add-to-cart text-center">
                                                <a href="#">ADD TO CART</a>
                                            </div>
                                            <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                        </div>
                                    </div><!--owl item end -->
                                </div>
                                <!-- /.owl-carousel  -->
                            </div>
                        </div>
                        <!-- /.flat-content-box -->
                        <div class="divider h97"></div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 half-background style-3" style="height: 719px;">
                    <div class="flat-image-box style-1 clearfix">
                        <div class="elm-btn">
                            <a href="#" class="themesflat-button bg-white width-150">For woMan’s</a>
                        </div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- END IMAGE BOX -->

    <!-- IMAGE BOX -->
    <section class="flat-row no-padding">
        <div class="container-fluid">
            <div class="row equal sm-equal-auto">
                <div class="col-md-6 half-background style-4" style="height: 719px;">
                    <div class="flat-image-box style-1 clearfix">
                        <div class="elm-btn">
                            <a href="#" class="themesflat-button bg-white width-150">For Kid’s</a>
                        </div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6" style="height: 719px;">
                    <div class="product-content clearfix">
                        <div class="divider h160"></div>
                        <div class="flat-content-box clearfix" data-margin="0 0px 0 0px" data-mobilemargin="0 0 0 0"
                             style="margin:0 0px 0 0px">
                            <div class="product style3 flat-carousel-box has-bullets bullet-circle bullet40 clearfix"
                                 data-auto="false" data-column="3" data-column2="1" data-column3="1" data-gap="0">
                                <div class="owl-carousel owl-theme owl-loaded owl-drag">

                                            <div class="owl-item" style="width: 215px;">
                                                <div class="product-item">
                                                    <div class="product-thumb clearfix">
                                                        <a href="#">
                                                            <img src="{{asset('img/12.jpg')}}" alt="image">
                                                        </a>
                                                        <span class="new">New</span>
                                                    </div>
                                                    <div class="product-info clearfix">
                                                        <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                        <div class="price">
                                                            <ins>
                                                                <span class="amount">$20.00</span>
                                                            </ins>
                                                        </div>
                                                    </div>
                                                    <div class="add-to-cart text-center">
                                                        <a href="#">ADD TO CART</a>
                                                    </div>
                                                    <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                                </div>
                                            </div>
                                    <div class="owl-item" style="width: 215px;">
                                        <div class="product-item">
                                            <div class="product-thumb clearfix">
                                                <a href="#">
                                                    <img src="{{asset('img/13.jpg')}}" alt="image">
                                                </a>
                                                <span class="new">New</span>
                                            </div>
                                            <div class="product-info clearfix">
                                                <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                <div class="price">
                                                    <ins>
                                                        <span class="amount">$19.00</span>
                                                    </ins>
                                                </div>
                                                <ul class="flat-color-list width-14">
                                                    <li>
                                                        <a href="#" class="red"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="blue"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="black"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="add-to-cart text-center">
                                                <a href="#">ADD TO CART</a>
                                            </div>
                                            <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                        </div>
                                    </div><!--owl item end -->
                                    <div class="owl-item" style="width: 215px;">
                                        <div class="product-item">
                                            <div class="product-thumb clearfix">
                                                <a href="#">
                                                    <img src="{{asset('img/13.jpg')}}" alt="image">
                                                </a>
                                                <span class="new">New</span>
                                            </div>
                                            <div class="product-info clearfix">
                                                <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                <div class="price">
                                                    <ins>
                                                        <span class="amount">$19.00</span>
                                                    </ins>
                                                </div>
                                                <ul class="flat-color-list width-14">
                                                    <li>
                                                        <a href="#" class="red"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="blue"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="black"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="add-to-cart text-center">
                                                <a href="#">ADD TO CART</a>
                                            </div>
                                            <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                        </div>
                                    </div><!--owl item end -->
                                </div><!-- /.owl-carousel  -->
                            </div>
                        </div>
                        <!-- /.flat-content-box -->
                        <div class="divider h97"></div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- END IMAGE BOX -->

    <!-- IMAGE BOX -->
    <section class="flat-row no-padding">
        <div class="container-fluid">
            <div class="row equal sm-equal-auto">
                <div class="col-md-6" style="height: 719px;">
                    <div class="product-content clearfix">
                        <div class="divider h160"></div>
                        <div class="flat-content-box clearfix" data-margin="0 0px 0 0px" data-mobilemargin="0 0 0 0"
                             style="margin:0 0px 0 0px">
                            <div class="product style3 flat-carousel-box has-bullets bullet-circle bullet40 clearfix"
                                 data-auto="false" data-column="3" data-column2="1" data-column3="1" data-gap="0">
                                <div class="owl-carousel owl-theme owl-loaded owl-drag">
                                            <div class="owl-item active" style="width: 215px;">
                                                <div class="product-item">
                                                    <div class="product-thumb clearfix">
                                                        <a href="#">
                                                            <img src="{{asset('img/19.jpg')}}" alt="image">
                                                        </a>
                                                        <span class="new">New</span>
                                                    </div>
                                                    <div class="product-info clearfix">
                                                        <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                        <div class="price">
                                                            <ins>
                                                                <span class="amount">$19.00</span>
                                                            </ins>
                                                        </div>
                                                        <ul class="flat-color-list width-14">
                                                            <li>
                                                                <a href="#" class="red"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="blue"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="black"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="add-to-cart text-center">
                                                        <a href="#">ADD TO CART</a>
                                                    </div>
                                                    <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                                </div>
                                            </div>
                                    <div class="owl-item" style="width: 215px;">
                                        <div class="product-item">
                                            <div class="product-thumb clearfix">
                                                <a href="#">
                                                    <img src="{{asset('img/18.jpg')}}" alt="image">
                                                </a>
                                                <span class="new">New</span>
                                            </div>
                                            <div class="product-info clearfix">
                                                <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                <div class="price">
                                                    <ins>
                                                        <span class="amount">$19.00</span>
                                                    </ins>
                                                </div>
                                                <ul class="flat-color-list width-14">
                                                    <li>
                                                        <a href="#" class="red"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="blue"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="black"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="add-to-cart text-center">
                                                <a href="#">ADD TO CART</a>
                                            </div>
                                            <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                        </div>
                                    </div><!--owl item end -->
                                    <div class="owl-item" style="width: 215px;">
                                        <div class="product-item">
                                            <div class="product-thumb clearfix">
                                                <a href="#">
                                                    <img src="{{asset('img/17.jpg')}}" alt="image">
                                                </a>
                                                <span class="new">New</span>
                                            </div>
                                            <div class="product-info clearfix">
                                                <span class="product-title">Cotton White Underweaer Block Out Edition</span>

                                                <div class="price">
                                                    <ins>
                                                        <span class="amount">$19.00</span>
                                                    </ins>
                                                </div>
                                                <ul class="flat-color-list width-14">
                                                    <li>
                                                        <a href="#" class="red"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="blue"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="black"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="add-to-cart text-center">
                                                <a href="#">ADD TO CART</a>
                                            </div>
                                            <a href="#" class="like"><i class="fa fa-heart-o"></i></a>
                                        </div>
                                    </div><!--owl item end -->
                                </div><!-- /.owl-carousel  -->
                            </div>
                        </div>
                        <!-- /.flat-content-box -->
                        <div class="divider h97"></div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
                <div class="col-md-6 half-background style-5" style="height: 719px;">
                    <div class="flat-image-box style-1 clearfix">
                        <div class="elm-btn">
                            <a href="#" class="themesflat-button bg-white width-150">accessories</a>
                        </div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- END IMAGE BOX -->

    <div class="divider h73 line-shadow"></div>

    <section class="flat-row mail-chimp">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="text">
                        <h3>Sign up for Send Newsletter</h3>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="subscribe clearfix">
                        <form action="#" method="post" accept-charset="utf-8" id="subscribe-form">
                            <div class="subscribe-content">
                                <div class="input">
                                    <input type="email" name="subscribe-email" placeholder="Your Email">
                                </div>
                                <div class="button">
                                    <button type="button">SUBCRIBE</button>
                                </div>
                            </div>
                        </form>
                        <ul class="flat-social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                            <li><a href="#"><i class="fa fa-behance"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                        <!-- /.flat-social -->
                    </div>
                    <!-- /.subscribe -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.mail-chimp -->

    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="widget widget-link">
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Online Store</a></li>
                            <li><a href="blog-list.html">Blog</a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                        </ul>
                    </div>
                    <!-- /.widget -->
                </div>
                <!-- /.col-md-3 -->
                <div class="col-sm-6 col-md-3">
                    <div class="widget widget-link link-login">
                        <ul>
                            <li><a href="#">Login/ Register</a></li>
                            <li><a href="#">Your Cart</a></li>
                            <li><a href="#">Wishlist items</a></li>
                            <li><a href="#">Your checkout</a></li>
                        </ul>
                    </div>
                    <!-- /.widget -->
                </div>
                <!-- /.col-md-3 -->
                <div class="col-sm-6 col-md-3">
                    <div class="widget widget-link link-faq">
                        <ul>
                            <li><a href="faqs.html">FAQs</a></li>
                            <li><a href="#">Term of service</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Returns</a></li>
                        </ul>
                    </div>
                    <!-- /.widget -->
                </div>
                <!-- /.col-md-3 -->
                <div class="col-sm-6 col-md-3">
                    <div class="widget widget-brand">
                        <div class="logo logo-footer">
                            <a href="index.html"><img src="{{asset('img/logo@2x.png')}}" alt="image" width="107" height="24"></a>
                        </div>
                        <ul class="flat-contact">
                            <li class="address">112 Kingdom, NA 12, New York</li>
                            <li class="phone">+12 345 678 910</li>
                            <li class="email">infor.deercreative@gmail.com</li>
                        </ul>
                        <!-- /.flat-contact -->
                    </div>
                    <!-- /.widget -->
                </div>
                <!-- /.col-md-3 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </footer>
    <!-- /.footer -->

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="copyright text-center">Copyright @2018 <a href="#">Modaz</a></p>
                </div>
            </div>
        </div>
    </div>

    <!-- Go Top -->
    <a class="go-top show">
        <i class="fa fa-chevron-up"></i>
    </a>

</div>

@include('inc.footer')