<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use CrudTrait;

    protected $table = 'products';

    protected $guarded = ['id'];


    public function category()
    {
        return $this->hasOne('App\Models\Category', 'category_id');
    }

    public function brand()
    {
        return $this->hasOne('App\Models\Brand', 'brand_id');
    }

}
