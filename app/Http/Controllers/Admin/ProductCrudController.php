<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\ProductRequest as StoreRequest;
use App\Http\Requests\ProductRequest as UpdateRequest;

class ProductCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/product');
        $this->crud->setEntityNameStrings('product', 'products');

        $this->crud->setFromDb();

        // start customizing here
        $this->crud->removeColumns(['active', 'slug', 'onSale', 'salePrice', 'img', 'thumbnails', 'description']); // remove an array of columns from the stack

        // add asterisk for fields that are required in TagRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->addField([
            'name'  => 'active',
            'label' => 'Active',
            'type'  => 'checkbox',
        ]);

        $this->crud->addField([
            'name'  => 'img',
            'label' => 'Product Image',
            'type'  => 'upload',
            'hint'       => 'Product Main Image',
            'wrapperAttributes' => [
                'class' => 'upload-field form-group col-md-12'
            ]
        ]);

        $this->crud->addField([
            'name'  => 'thumbnails',
            'label' => 'Product Thumbnails',
            'type'  => 'upload_multiple',
            'wrapperAttributes' => [
                'class' => 'upload-field form-group col-md-12'
            ]
        ]);

        $this->crud->addField([
            'name'  => 'salePrice',
            'label' => 'Sale Price',
            'type'  => 'number',
        ]);

        $this->crud->addField([
            'name'  => 'stockQty',
            'label' => 'Stock Qty',
            'type'  => 'number',
        ]);

        $this->crud->addField([
            'name'  => 'onSale',
            'label' => 'Is On Sale',
            'type'  => 'checkbox',
        ]);

        $this->crud->addField([
            'name'  => 'description',
            'label' => 'Product Description',
            'type'  => 'wysiwyg',
        ]);

        $this->crud->addField(
            [  // Select2
            'label' => "Brand",
            'type' => 'select2',
            'name' => 'brand_id', // the db column for the foreign key
            'entity' => 'brand', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\\Models\\Brand" // foreign key model
            ]
        );

        $this->crud->addField(
            [  // Select2
                'label' => "Category",
                'type' => 'select2',
                'name' => 'category_id',
                'entity' => 'category',
                'attribute' => 'name',
                'model' => "App\\Models\\Category"
            ]
        );

    }

    public function store(StoreRequest $request)
    {
        dd($request);
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
